import React, { Component } from "react";
import axios from "axios";
const urlAPI = "http://backendexample.sanbercloud.com/api/movies";

export default class Home extends Component {
  constructor() {
    super();
    this.state = { movies: [] };
  }
  componentDidMount() {
    if (this.state.movies.length === 0) {
      axios.get(urlAPI).then((res) => {
        const movies = res.data;
        this.setState({ movies });
      });
    }
  }
  render() {
    console.log(this.state.movies);
    const movies = this.state.movies.slice(0);
    movies.sort((a, b) => (a.rating < b.rating ? 1 : -1));
    return (
      <section>
        <h1>Daftar Film Film Terbaik</h1>

        {movies.map((movie, index) => (
          <div>
            <h3
              href="/"
              style={{ padding: 0, marginBottom: 5, color: "#003366" }}
            >
              {movie.title} (Year: {movie.year})
            </h3>
            <p style={{ padding: 0, margin: 0, fontSize: 14 }}>
              <strong>
                Rating : {movie.rating}
                <br />
                Durasi : {Math.round(movie.duration / 60)} Jam{" "}
                {movie.duration % 60} menit
                <br />
                Genre : {movie.genre}
                <br />
              </strong>
            </p>
            <p style={{ padding: 0, marginTop: 5, fontSize: 14 }}>
              <strong>Deskripsi </strong>: {movie.description}
            </p>
          </div>
        ))}
      </section>
    );
  }
}
