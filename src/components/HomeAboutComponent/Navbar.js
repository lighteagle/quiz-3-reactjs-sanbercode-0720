import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import Home from "./Home";
import About from "./About";

export default function Navbar() {
  return (
    <>
      <header>
        <img id="logo" src={"/img/logo.png"} width={200} alt="logo" />
        <nav>
          <ul>
            <li>
              <Link to="/">Home </Link>
            </li>
            <li>
              <Link to="/about">About </Link>
            </li>
            <li>
              <Link to="/">Movie List Editor </Link>
            </li>
          </ul>
        </nav>
      </header>
      <Switch>
        <Route exact path="/" component={Home}></Route>
        <Route path="/about" component={About} />
      </Switch>
    </>
  );
}
