import React, { useContext, useEffect } from "react";
import { MovieDataContext } from "./MovieDataContext";
import axios from "axios";
const urlAPI = "http://backendexample.sanbercloud.com/api/movies";

const MovieDataEdit = () => {
  const [movieData, setMovieData, input, setInput] = useContext(
    MovieDataContext
  );
  useEffect(() => {
    axios.get(urlAPI).then((res) => {
      setMovieData(res.data);
    });
  }, [setMovieData, input.changeData]);

  const handleChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case "title":
        setInput({ ...input, title: value });
        break;
      case "year":
        setInput({ ...input, year: value });
        break;
      case "duration":
        setInput({ ...input, duration: value });
        break;
      case "genre":
        setInput({ ...input, genre: value });
        break;
      case "rating":
        if (value >= 1 && value <= 10) setInput({ ...input, rating: value });
        break;
      case "description":
        setInput({ ...input, description: value });
        break;

      default:
        break;
    }
    console.log(input);
  };

  const handleDelete = (event) => {
    const movieId = parseInt(event.target.value);
    axios
      .delete(`${urlAPI}/${movieId}`)
      .then((res) => {
        console.log(res);
      })
      .then(() => setInput({ ...input, changeData: input.changeData + 1 }));
  };
  const handleEdit = (event) => {
    const selectedId = event.target.value;
    const {
      title,
      year,
      duration,
      genre,
      rating,
      description,
    } = movieData.find((x) => x.id === parseInt(selectedId));
    setInput({ title, year, duration, genre, rating, description, selectedId });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const {
      title,
      year,
      duration,
      genre,
      rating,
      description,
      selectedId,
    } = input;

    if (selectedId === -1) {
      axios
        .post(urlAPI, { title, year, duration, genre, rating, description })
        .then((res) => {
          console.log(res.data);
          setInput({ ...input, changeData: input.changeData + 1 });
        })
        .then(() =>
          setInput({
            ...input,
            title: "",
            year: 2000,
            duration: 1,
            genre: "",
            rating: 1,
            description: "",
            selectedId: -1,
          })
        );
    } else {
      axios
        .put(`${urlAPI}/${parseInt(selectedId)}`, {
          title,
          year,
          duration,
          genre,
          rating,
          description,
        })
        .then((res) => {
          console.log(res);
          setInput({ ...input, changeData: input.changeData + 1 });
        })
        .then(() =>
          setInput({
            ...input,
            title: "",
            year: 2000,
            duration: 1,
            genre: "",
            rating: 1,
            description: "",
            selectedId: -1,
          })
        );
    }
  };

  return (
    <section>
      <h1>Movie List Editor</h1>
      <div style={{ width: "50%", margin: "10px auto", display: "block" }}>
        <div style={{ border: "1px solid #aaa", padding: "20px" }}>
          <form onSubmit={handleSubmit}>
            <label style={{ float: "left" }}>Title:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="title"
              value={input.title}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Year:</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="year"
              value={input.year}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Duration (dalam menit):</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="duration"
              value={input.duration}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Genre:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="genre"
              value={input.genre}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Rating (1-10):</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="rating"
              value={input.rating}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Descriptions:</label>
            <textarea
              style={{ float: "right", width: 400, height: 80 }}
              name="description"
              value={input.description}
              onChange={handleChange}
            />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div style={{ width: "100%", paddingBottom: "20px" }}>
              <button style={{ float: "right" }}>submit</button>
            </div>
          </form>
        </div>
      </div>
      <div>
        <table style={{ margin: "auto" }}>
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Year</th>
              <th>Duration (menit)</th>
              <th>Genre</th>
              <th>Rating (1 - 10)</th>
              <th>Descriptions</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody style={{ fontSize: 12 }}>
            {movieData.map((movie, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{movie.title}</td>
                <td>{movie.year}</td>
                <td>{movie.duration}</td>
                <td>{movie.genre}</td>
                <td>{movie.rating}</td>
                <td>
                  {movie.description &&
                    (movie.description.length > 50
                      ? movie.description.substring(0, 45) + " ..."
                      : movie.description)}
                </td>
                <td>
                  <button onClick={handleEdit} value={movie.id}>
                    Edit
                  </button>
                  &nbsp;
                  <button onClick={handleDelete} value={movie.id}>
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </section>
  );
};

export default MovieDataEdit;
