import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { MovieDataContext } from "./MovieDataContext";

const Login = () => {
  const history = useHistory();
  const [movieData, setMovieData, input, setInput] = useContext(
    MovieDataContext
  );
  const [userName, setUserName] = useState("");
  const handleChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case "name":
        setUserName(value);
        break;
      default:
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setInput({ ...input, userName, isLogin: true });
    history.push("/");
  };
  return (
    <section>
      <h1 style={{ textAlign: "center" }}>Login</h1>
      <div>
        <div
          style={{ display: "block", marginBottom: "1em", textAlign: "center" }}
        >
          <div
            style={{
              display: "inline-block",
              width: 150,
              fontWeight: "bold",
              fontSize: 16,
            }}
          >
            Nama
          </div>

          <input
            style={{ display: "inline-block" }}
            type="text"
            name="name"
            value={userName}
            onChange={handleChange}
          />
        </div>
        <div
          style={{ display: "block", marginBottom: "1em", textAlign: "center" }}
        >
          <div
            style={{
              display: "inline-block",
              width: 150,
              fontWeight: "bold",
              fontSize: 16,
            }}
          >
            Email
          </div>

          <input style={{ display: "inline-block" }} type="text" name="email" />
        </div>
        <div style={{ textAlign: "center" }}>
          <button onClick={handleSubmit}>Login</button>
        </div>
      </div>
    </section>
  );
};

export default Login;
