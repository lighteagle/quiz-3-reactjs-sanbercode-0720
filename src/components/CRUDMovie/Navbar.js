import React, { useContext } from "react";
import { Switch, Link, Route, useHistory } from "react-router-dom";
import MovieDataList from "./MovieDataList";
import Login from "./Login";
import { MovieDataContext } from "./MovieDataContext";
import About from "./About";
import MovieDataEdit from "./MovieDataEdit";

const Navbar = () => {
  const history = useHistory();
  const [movieData, setMovieData, input, setInput] = useContext(
    MovieDataContext
  );
  return (
    <>
      <header>
        <img id="logo" src={"/img/logo.png"} width={200} alt="logo" />
        <nav>
          <ul>
            <li>
              <Link to="/">Home </Link>
            </li>
            <li>
              <Link to="/about">About </Link>
            </li>
            {input.isLogin ? (
              <>
                <li>
                  <Link to="/movie-edit">Movie List Editor </Link>
                </li>
                <li>Welcome, {input.userName}</li>
                <li>
                  <div
                    onClick={() => {
                      setInput({ ...input, isLogin: false, userName: "" });
                      history.push("/");
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    Logout
                  </div>
                </li>
              </>
            ) : (
              <li>
                <Link to="/login">
                  {input.isLogin ? `Welcome, ${input.userName}` : "Login"}
                </Link>
              </li>
            )}
          </ul>
        </nav>
      </header>
      <Switch>
        <Route path="/login" component={Login}></Route>
        <Route path="/about" component={About}></Route>
        <Route exact path="/" component={MovieDataList}></Route>
        <Route path="/movie-edit" component={MovieDataEdit}></Route>
      </Switch>
    </>
  );
};

export default Navbar;
