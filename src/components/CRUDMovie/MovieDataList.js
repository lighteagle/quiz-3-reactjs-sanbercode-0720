import React, { useContext, useEffect } from "react";
import { MovieDataContext } from "./MovieDataContext";
import axios from "axios";

const urlAPI = "http://backendexample.sanbercloud.com/api/movies";

const MovieDataList = () => {
  const [movieData, setMovieData, input, setInput] = useContext(
    MovieDataContext
  );

  useEffect(() => {
    axios.get(urlAPI).then((res) => {
      // console.log(res.data);
      const sortRating = res.data.slice(0);
      sortRating.sort((a, b) => (a.rating < b.rating ? 1 : -1));
      setMovieData(sortRating);
    });
  }, [setMovieData]);
  return (
    <section>
      <h1>
        Selamat Datang {input.isLogin ? input.userName : "Guest"}, Berikut
        adalah data Film Terbaik
      </h1>
      {movieData.map((movie, index) => (
        <div>
          <h3
            href="/"
            style={{ padding: 0, marginBottom: 5, color: "#003366" }}
          >
            {movie.title} (Year: {movie.year})
          </h3>
          <p style={{ padding: 0, margin: 0, fontSize: 14 }}>
            <strong>
              Rating : {movie.rating}
              <br />
              Durasi : {Math.round(movie.duration / 60)} Jam{" "}
              {movie.duration % 60} menit
              <br />
              Genre : {movie.genre}
              <br />
            </strong>
          </p>
          <p style={{ padding: 0, marginTop: 5, fontSize: 14 }}>
            <strong>Deskripsi </strong>: {movie.description}
          </p>
        </div>
      ))}
    </section>
  );
};

export default MovieDataList;
