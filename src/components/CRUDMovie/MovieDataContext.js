import React, { useState, createContext } from "react";

export const MovieDataContext = createContext([]);

export const MovieDataProvider = (props) => {
  const [movieData, setMovieData] = useState([]);
  const [input, setInput] = useState({
    title: "",
    year: 2000,
    duration: 1,
    genre: "",
    rating: 1,
    description: "",
    selectedId: -1,
    changeData: 0,
    isLogin: false,
    userName: "",
  });

  return (
    <MovieDataContext.Provider
      value={[movieData, setMovieData, input, setInput]}
    >
      {props.children}
    </MovieDataContext.Provider>
  );
};
