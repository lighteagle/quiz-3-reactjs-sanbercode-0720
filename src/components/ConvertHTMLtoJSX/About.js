import React from "react";

const About = () => {
  return (
    <section>
      <div style={{ padding: 10, border: "1px solid #ccc" }}>
        <h1 style={{ textAlign: "center" }}>
          Data Peserta Sanbercode Bootcamp Reactjs
        </h1>
        <ol>
          <li>
            <strong style={{ width: 100 }}>Nama:</strong> Sofyan Agus
          </li>
          <li>
            <strong style={{ width: 100 }}>Email:</strong>{" "}
            sofyan.agus18@gmail.com
          </li>
          <li>
            <strong style={{ width: 100 }}>
              Sistem Operasi yang digunakan:
            </strong>{" "}
            Windows
          </li>
          <li>
            <strong style={{ width: 100 }}>Akun Gitlab: </strong>
            @lighteagle
          </li>
          <li>
            <strong style={{ width: 100 }}>Akun Telegram: </strong>
            +62182186994599
          </li>
        </ol>
      </div>
    </section>
  );
};

export default About;
