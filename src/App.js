import React from "react";
import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import ConvertHTMLtoJSX from "./components/ConvertHTMLtoJSX/Navbar";
import HomeClassComponent from "./components/HomeAboutComponent/Navbar";
import CRUDMovie from "./components/CRUDMovie/Navbar";
import { MovieDataProvider } from "./components/CRUDMovie/MovieDataContext";

function App() {
  return (
    <Router>
      <MovieDataProvider>
        {/* No.2 dan No.3 */}
        {/* <ConvertHTMLtoJSX /> */}
        {/* No.4  */}
        {/* <HomeClassComponent /> */}
        {/* No.5 */}
        <CRUDMovie />
      </MovieDataProvider>
    </Router>
  );
}

export default App;
